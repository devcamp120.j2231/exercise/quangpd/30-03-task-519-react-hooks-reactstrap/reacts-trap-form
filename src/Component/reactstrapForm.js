import dog from "../assets/images/dog.jpg";
import { Button, Col, Input, Label, Row } from "reactstrap";

const ReactsTrapForm = () =>{
    return(
        <>
            <div className="container border border-warning">
                <Row>
                    <Col sm={12}>
                        <h4 className="text-center">Hồ Sơ Nhân Viên</h4>
                    </Col>
                </Row>
                <Row className="mt-4">
                    <Col sm={8} >
                        <Row className="form-group mt-3">
                            <Col sm={3} >
                                <Label> Họ và tên</Label>
                            </Col>
                            <Col sm={9} >
                                <Input></Input>
                            </Col>
                        </Row>
                        <Row className="form-group mt-3">
                            <Col sm={3} >
                                <Label>Ngày sinh</Label>
                            </Col>
                            <Col sm={9} >
                                <Input></Input>
                            </Col>
                        </Row>
                        <Row className="form-group mt-3">
                            <Col sm={3} >
                                <Label>Số điện thoại</Label>
                            </Col>
                            <Col sm={9} >
                                <Input></Input>
                            </Col>
                        </Row>
                        <Row className="form-group mt-3">
                            <Col sm={3} >
                                <Label>Giới tính</Label>
                            </Col>
                            <Col sm={9} >
                                <Input></Input>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={4}>
                        <img src={dog} className="text-center" height={"300px"} width={"400px"} alt="profilePic"/>
                    </Col>
                </Row>
                <Row className="mt-4">
                    <Col sm={2}>
                        <Label>Công việc</Label>
                    </Col>
                    <Col sm={10}>
                        <textarea className="form-control"></textarea>
                    </Col>
                </Row>
                <Row className="mt-4">
                   <Col sm={12} style={{paddingLeft : "900px"}}>
                        <Button className="btn btn-success" style={{marginRight : "30px"}}>Chi Tiết</Button>
                        <Button className="btn btn-success">Kiểm tra</Button>
                   </Col>
                </Row>
            </div>

        </>
    )
}

export default ReactsTrapForm